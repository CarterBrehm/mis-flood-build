﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using UnityEngine.AI;

public class SaveThosebois : MonoBehaviour
{


    private GameObject[] helis, savePositions, liftSpot, landSpot;
    List<GameObject> savPos = new List<GameObject>();
    List<Vector3> scales = new List<Vector3>();
    List<Transform> lands = new List<Transform>();
    List<Vector3> lifts = new List<Vector3>();
    List<Vector3> end = new List<Vector3>();
    private float speed = 0.1f;
    public AnimationCurve pitchCurve;
    private float tilt = 0.1f;
    private enum state { liftoff, gosaveboi, returntosaveboi, end };
    state heliState = state.liftoff;
    private Quaternion Rotate;
    private Quaternion blarg;
    private Quaternion targetRotation;
    private Vector3 rotAngle;
    private float distance;
    private float angle;
    private bool startRescue = false;
    private GameObject[] r;
    GameObject e;
    epicgamer epic;
    Vector3 rotateDir;
    public int yaet = 1;
    public float RescueHeight;
    Random rand = new Random();




    // Start is called before the first frame update
    void Start()
    {
        //get all locations and helicopters
        liftSpot = GameObject.FindGameObjectsWithTag("lift");
        landSpot = GameObject.FindGameObjectsWithTag("landspot");
        helis = GameObject.FindGameObjectsWithTag("heli");
        for (int i = 0; i < helis.Length; i++)
        {
            scales.Add(helis[i].transform.localScale);
            helis[i].transform.localScale = new Vector3(0f, 0f, 0f);
            lifts.Add(helis[i].transform.position + new Vector3(0f, 25f, 0f));
            lands.Add(helis[i].transform);
        }
        //get all positions save those bois
        savePositions = GameObject.FindGameObjectsWithTag("savePosition");
        foreach (GameObject sp in savePositions)
        {
            savPos.Add(sp);
        }
        setSavePositions();
        //gets rotors and epicgamer script to start animations at whatever point
        r = GameObject.FindGameObjectsWithTag("rotor");
        e = GameObject.FindGameObjectWithTag("MainCamera");
        epic = (epicgamer)e.GetComponent(typeof(epicgamer));
    }

    // Update is called once per frame
    void Update()
    {
        //for loop to do this stuff for all helicopters
        for (int i = 0; i < helis.Length; i++)
        {
            //if helicopters are upgraded, new helicopter appears
            if (yaet <= helis.Length)
            {
                getNewHeli();
            }
            if (startRescue == true)
            {
                switch (heliState)
                {
                    //lift off ground
                    case state.liftoff:
                        //once it's close to a certain height it will switch to go to a random point on the map
                        if (Vector3.Distance(helis[i].transform.position, lifts[i]) < 0.5f)
                        {
                            heliState = state.gosaveboi;
                            setDistanceAndAngle(helis[i].transform.position, end[i]);
                            speed = 0.01f;
                            tilt = 0.01f;
                            break;
                        }
                         helis[i].transform.position = Vector3.Lerp(helis[i].transform.position, lifts[i], speed * Time.deltaTime);
                         smoothSpeed();

                        break;
                    //go to random point on map
                    case state.gosaveboi:
                        //once close enough, switch to return to starting point
                        if (Vector3.Distance(helis[i].transform.position, end[i]) < 0.5f)
                        {
                            heliState = state.returntosaveboi;
                            setDistanceAndAngle(helis[i].transform.position, lifts[i]);
                            speed = 0.1f;
                            tilt = 0.1f;
                            break;
                        }
                        move(helis[i], end[i]);
                        smoothTilt();
                        smoothSpeed();

                        break;
                    //returns to starting point on map
                    case state.returntosaveboi:
                        //once close to starting point turns around and goes to another random point on the map (switches back to state.gosaveboi to be in a loop until the end of game
                        if (Vector3.Distance(helis[i].transform.position, lifts[i]) < 0.5f)
                        {
                            if (epic.getGameState() != epicgamer.state.post)
                            {
                                setSavePositions();
                                heliState = state.gosaveboi;
                                setDistanceAndAngle(helis[i].transform.position, end[i]);
                                speed = 0.1f;
                                tilt = 0.1f;
                                break;
                            }
                            //if the game is over, it will land on its starting point on the ground
                            else
                            {
                                setDistanceAndAngle(helis[i].transform.position, lands[i].position);
                                speed = 0.1f;
                                tilt = 0.1f;
                                heliState = state.end;
                                break;
                            }
                        }
                        move(helis[i], lifts[i]);
                        smoothTilt();
                        smoothSpeed();

                        break;
                    //lands on starting point on ground
                    case state.end:
                        //stops moving
                        if (Vector3.Distance(helis[i].transform.position, lands[i].position) < 0.1f)
                        {
                            break;
                        }
                        helis[i].transform.position = Vector3.Lerp(helis[i].transform.position, lands[i].position, speed * Time.deltaTime);
                        helis[i].transform.rotation = Quaternion.Slerp(helis[i].transform.rotation, lands[i].rotation, speed * Time.deltaTime);
                        smoothSpeed();

                        break;
                }

            }
        }
    }

    //move h to position posish on map
    public void move(GameObject h, Vector3 posish)
    {
        float i;
        rotAngle = (posish - h.transform.position);

        //if the angle is a certain way it will turn the helicopter one way or the other
        if (Vector3.Angle(h.transform.forward, rotAngle) < 180f)
        {
            rotateDir = Vector3.up;
            i = -1f;
        }
        else
        {
            rotateDir = -Vector3.up;
            i = 1f;
        }

        //sets target rotation on y axis
        targetRotation = h.transform.rotation * Quaternion.AngleAxis((Vector3.Angle(h.transform.forward, rotAngle)), rotateDir);
        //rotates to the target rotation
        Rotate = Quaternion.Slerp(h.transform.rotation, targetRotation, speed * Time.deltaTime);
        //makes it so that it rotates ONLY on the y axis
        blarg = Quaternion.Euler(new Vector3(h.transform.eulerAngles.x, Rotate.eulerAngles.y, h.transform.eulerAngles.z));
        //does the animation
        h.transform.rotation = blarg;

        //rotates helicopters on the x and z axis for a more realistic effect (x pitches the head down and z rotates the helicopter around as if you were facing it straight on
        h.transform.eulerAngles = new Vector3(
                pitchCurve.Evaluate(Vector3.Distance(h.transform.position, posish) / getDistance()) * tilt,  //uses distance
                h.transform.eulerAngles.y,
                pitchCurve.Evaluate(Vector3.Angle(h.transform.forward, rotAngle) / getAngle()) * tilt * i);  //uses angles

        h.transform.position = Vector3.Lerp(h.transform.position, posish, speed * Time.deltaTime);
    }

    //sets distance and angle of starting position to save point
    public void setDistanceAndAngle(Vector3 a, Vector3 b)
    {
        distance = Vector3.Distance(a, b);
        rotAngle = (b - a);
        angle = Vector3.Angle(a, rotAngle);
    }

    public float getDistance()
    {
        return distance;
    }

    public float getAngle()
    {
        return angle;
    }

    public void smoothSpeed()
    {
        if (speed <= 1)
        {
            speed += Mathf.Pow(0.015f, 2f);
        }
    }

    //smoothspeed and smoothtilt start at very low values and gradually increase at the start of any given movement for smoother transitions

    public void smoothTilt()
    {

        if (tilt <= 20f)
        {
            tilt += Mathf.Pow(.15f, 2f);
        }
    }

    //sets the point on the map which the helicopter will go to
    public void setEndPosition(Vector3 end, Vector3 p)
    {
        end = p;
    }

    //sets random positions on map for all helicopters to go to each time
    public void setSavePositions()
    {
        end.Clear();
        for (int i = 0; i < helis.Length; i++)
        {
            Vector3 oof = new Vector3(0f, RescueHeight, 0f);
            GameObject opa = savePositions[rand.Next(0, savPos.Count)];
            Vector3 oma = opa.GetComponent<Transform>().transform.position + oof;
            end.Add(oma);
            if (i > 0 && helis[helis.Length - 1] == helis[i - 1])
            {
                setSavePositions();
            }
        }
    }

    //starts all animations in update()
    public void startHeliRescue()
    {
        startRescue = true;
    }

    //adds new helicopter
    public void addNewBoi()
    {
        yaet++;
    }

    //does animation for adding new helicopter
    public void getNewHeli()
    {
        for (int i = 0; i < yaet; i++)
        {
            if (helis[i].transform.localScale.magnitude <= (scales[i].magnitude / 1.005))
            {
                helis[i].transform.localScale = Vector3.Lerp(helis[i].transform.localScale, scales[i], 0.5f * Time.deltaTime);
            }
            else { }
        }
    }

    public void stopthoseAnims()
    {
        foreach (GameObject Rotor in r)
        {
            Rotor.GetComponent<Animator>().enabled = false;
        }
    }

    public void startthoseAnims()
    {
        foreach (GameObject Rotor in r)
        {
            if (Rotor.GetComponent<Animator>().enabled == false)
            {
                Rotor.GetComponent<Animator>().enabled = true;
            }
            if (Rotor.GetComponent<Animator>().enabled == true)
            {
                Rotor.GetComponent<Animator>().Play("Rotor");
                Rotor.GetComponent<Animator>().Play("Rotor_Back");
            }
        }
    }

}
