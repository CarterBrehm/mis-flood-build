﻿using System.Collections.Generic;
using UnityEngine;

public class StretchRealGood : MonoBehaviour {

    //this is incremented up by the mastermaster script epicgamer whenever the walls are upgraded to track when it should change from wood to stone
    public int again = 0;

    //array of dam and levee objects that are pre-made and placed
	GameObject[] dulevee;
    
    //list of transforms attached to the dam and levee objects
    List<Transform> ldtransform = new List<Transform>();

    //list of instantiated objects (wood and stone) for the dam and levee
    List<GameObject> wall = new List<GameObject>();

    //wood wall and stone wall prefabs
    public GameObject wood;
    public GameObject stone;

    //the instantiated object in the dam and levee that will "break" off
    GameObject breakDam;
    GameObject breakLevee;


    void Start() {
        //getting all levee/dam objects that were pre made and placed
		dulevee = GameObject.FindGameObjectsWithTag("levanddam");
        foreach (GameObject ld in dulevee)
        {
            //adding all transforms from the premade and placed levee/dam objects
            ldtransform.Add(ld.transform);

            //sets all premad stuff to inactive because they are only needed in the beginning
            ld.SetActive(false);
        }

        //calls the method that makes the levee and dam (made of wood)
        Walls(wood);
	}

    
    //nothing here to stop any sort of lag
    void Update() {
	}

    //gets the objects in dam and levee walls that will ultimately "break" off
    public void getWallsThatShallBreak()
    {

        //these do the same things but in two different places
        //it creates a collider at a specific place with a certain radius
        //the collider creates an array of stuff within itself
        //the array is then searched for either the wood or stone walls
        //when it finds the wood or stone, it sets that object at the location of the collider to variable
        //the variable is later used to lerp the object its assigned to to make the "breaking" animations

        Collider[] a = Physics.OverlapSphere(new Vector3(-553.5f, 22.6f, 354.4f), 1f);
        if (a.Length != 0)
        {
            foreach (Collider c in a)
            {
                if (c.gameObject.ToString() == "Wall-Wood (1)(Clone) (UnityEngine.GameObject)" || c.gameObject.ToString() == "Wall-Stone (1)(Clone) (UnityEngine.GameObject)")
                {
                    breakDam = c.gameObject;
                    break;
                }
            }
        }

        Collider[] b = Physics.OverlapSphere(new Vector3(-623.26f, 16.85f, 619.1f), 1f);
        if (b.Length != 0)
        {
            foreach (Collider c in b)
            {
                if (c.gameObject.ToString() == "Wall-Wood (1)(Clone) (UnityEngine.GameObject)" || c.gameObject.ToString() == "Wall-Stone (1)(Clone) (UnityEngine.GameObject)")
                {
                    breakLevee = c.gameObject;
                }
            }

        }
    }

    //makes new wall with whatever material and width
    public void Walls(GameObject wallmaterial)
    {
        //clears wal list of all stuff
        wall.Clear();
        for (int i = 0; i < ldtransform.Count; i++)
        {
            //instantiates a new object made of wallmaterial (wood or stone)
            //at the position of one of the premade things but 10 spaces up cuz the prefabs are small bois
            //its also with the rotation of the premade thingy
            wall.Add(Instantiate(wallmaterial, ldtransform[i].position + new Vector3(0f, 10f, 0f), ldtransform[i].rotation) as GameObject);

            //this sets the newly made wall of wood or stone's transform it is refencing to the transform it was made from
            wall[i].GetComponent<wallSlave>().ldtransform = ldtransform[i];

            //if the newly made thing is made of wood, set width to 10 for a thin width
            if (wallmaterial == wood)
            {
                wall[i].GetComponent<wallSlave>().width = 10f;
            }

            //if the thing is made of stone it sets the width to 100 for a thicc width
            else
            {
                wall[i].GetComponent<wallSlave>().width = 100f;
            }
        }
    }

    public void StratchThatBui() {
        //sets a new stretch point for the dam and levees
		for (var i = 0; i < wall.Count; i++)
        {
            wall[i].GetComponent<wallSlave>().stretchscale = wall[i].GetComponent<wallSlave>().stretchscale + 10f;
        }
	}

    public void Walldown()
    {
        //WHEN THEY ARE WOOD
        //changes all dam and levee objects' states to make it shrink
        //does this so that the next walls made of stone can be set in place
        for (var i = 0; i < wall.Count; i++)
        {
            wall[i].GetComponent<wallSlave>().wallState = wallSlave.state.wallGoDown;
        }
    }

    public void breakWall()
    {
        //transforms the dam object that will break off by bringing it forward and tilting it forward
        breakDam.transform.position = Vector3.Lerp(breakDam.transform.position, new Vector3(-542.4f, 5.6f, 368.5f), 1f * Time.deltaTime);
        breakDam.transform.rotation = Quaternion.Slerp(breakDam.transform.rotation, new Quaternion(1.0f, 0.3f, -0.45f, 1.0f), 1f * Time.deltaTime);

        //same here but with the levee instead
        //also dont touch this cuz its good now and i hate it
        breakLevee.transform.position = Vector3.Lerp(breakLevee.transform.position, new Vector3(-600.4f, 5f, 617.7f), 1f * Time.deltaTime);
        breakLevee.transform.rotation = Quaternion.Slerp(breakLevee.transform.rotation, new Quaternion(0.7f, 0.85f, -0.8f, 0.7f), 1f * Time.deltaTime);
    }

}