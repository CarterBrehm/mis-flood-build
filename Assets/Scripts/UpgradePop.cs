﻿using System.Collections.Generic;
using UnityEngine;

public class UpgradePop : MonoBehaviour {

    //an int that increses if all buildings are out to then start stretching them
	int again = 0;

    //array for all buildings
	GameObject[] buildings;

    //int so that multiple buildings can grow up at the same time
	int multiple = 0;

    //list of the scale of all the buildings
	readonly List<Vector3> scales = new List<Vector3>();

    //lerp speed
	readonly float speed = 1.5f;

    //factor by which all buildings will stretch by once they are stretched
	public float stretchfactor = 1f;
    
    //list of the scales all buildings will go to when stretched
    readonly List<Vector3> stretchscale = new List<Vector3>();

	void Start() {
        //gets all buildings
		buildings = GameObject.FindGameObjectsWithTag("building");

        foreach (var build in buildings) {
            //sets all buildings inactive
            build.SetActive(false);
            //gets all building scales(height, width, depth)
            scales.Add(build.transform.localScale);
            //sets all buildings' scales to 0
            build.transform.localScale = new Vector3(0f, 0f, 0f);
		}
	}

	void Update() {
        //called every frame since it lerps all the buildings
		popadoodle();
	}

	public void popThatBui() {
		{
			for (var i = 0; i < buildings.Length; i++)
                //does nothing if building already active
				if (buildings[i].activeInHierarchy) {
				} else {
                    //sets next building active then breaks the loop so that only one is set at a time
					buildings[i].SetActive(true);
					break;
				}
		}
	}

	public void popadoodle() {
        //conditional to not break code (outside of index)
		if (multiple <= buildings.Length)
			for (var i = 0; i < multiple; i++)
				if (buildings[i].transform.localScale.magnitude <= scales[i].magnitude / 1.005)
                    //lerps new building to full size if it is less than full size
					buildings[i].transform.localScale = Vector3.Lerp(buildings[i].transform.localScale, scales[i], speed * Time.deltaTime);
                //else do nothing
                else { }
		else
			for (var i = 0; i < buildings.Length; i++)
                //once all buildings are active then it starts to stretch all buildings with lerp
				buildings[i].transform.localScale = Vector3.Lerp(buildings[i].transform.localScale, stretchscale[i], speed * Time.deltaTime);
	}

	public void MultipleAtATime() {
        //increase multiple to all multiple building to go up at a time
		if (multiple <= buildings.Length) multiple++;
        //else it will start to stretch the boys
		if (multiple > buildings.Length) {
			again++;
			streeeeetch();
		}
	}

	public void streeeeetch() {
        
		if (stretchscale.Count < buildings.Length) {
            //adds new scales that are slightly taller than what the buildings are now
			for (var i = 0; i < buildings.Length; i++) stretchscale.Add(buildings[i].transform.localScale + new Vector3(0f, stretchfactor, 0f));
        //when there are already stretchscales then do this
		} else {
            //clears stretchscale of all stuff
			stretchscale.Clear();
            //calls this method again to get new scales for all buildings
			streeeeetch();
		}
	}
}