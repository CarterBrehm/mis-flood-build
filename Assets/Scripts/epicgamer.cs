using System;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;


public static class RandomExtensions
{
	public static double NextDouble(
		this Random random,
		double minValue,
		double maxValue)
	{
		return random.NextDouble() * (maxValue - minValue) + minValue;
	}
}

public class epicgamer : MonoBehaviour {
	
	zoooooooooooooooom zoom;
	GameObject blurPanel, preCanvas, buildCanvas, chopperPanel, stationPanel, downtownPanel, floodCanvas, postCanvas, leveePanel, evacPanel, stationPanel1, stationPanel2, stationPanel3;
	GameObject breakLev, damlev, dtown, flooder, helitown;
	StretchRealGood duleveee;
	flooding fludd;
	AnimationCurve rainCurve;
	Random random = new Random();
	UpgradePop upDowntown;
	master heliMaster;
	
	//global game state
	public enum state {
		pre,
		build,
		flood,
		post
	}
	state gameState = state.pre;
	
	// money
	double cash;
		// earning
		int cashPerDay = 200;
		int cashPerPerson = 200;
		// spending
		int downtownCost = 200;
		int stationCost = 500;
		int leveeCost = 100;
		int rescueCost = 100;
	
	// player started evac
	bool earlyEvac = false;

	//time
	double day = 0.0;
	int endDay;
	public double timeSpeed = 60;
	
	// people
	int people;
	int saved;
	int dead;
	
	// leveling
	int downtownLevel;
	int stationLevel;
	int leveeLevel;
	int rescueLevel;
	
	// misc iterators
	int i = 0;
	double x = 0;

	void Awake() {
		//assign UI empties to objects because tags are intensive
		preCanvas = GameObject.FindGameObjectWithTag("canvas-pre");
		buildCanvas = GameObject.FindGameObjectWithTag("canvas-build");
		floodCanvas = GameObject.FindGameObjectWithTag("canvas-flood");
		postCanvas = GameObject.FindGameObjectWithTag("canvas-post");
		blurPanel = GameObject.FindGameObjectWithTag("blur");
		//canvasGroup = mainCanvas.GetComponent<CanvasGroup>();
		//mainCanvas = GameObject.FindGameObjectWithTag("main-canvas");

		downtownPanel = GameObject.FindGameObjectWithTag("panel-downtown");
		leveePanel = GameObject.FindGameObjectWithTag("panel-levee");
		chopperPanel = GameObject.FindGameObjectWithTag("panel-chopper");

		stationPanel1 = GameObject.FindGameObjectWithTag("station-panel-1");
		stationPanel2 = GameObject.FindGameObjectWithTag("station-panel-2");
		stationPanel3 = GameObject.FindGameObjectWithTag("station-panel-3");
		stationPanel = GameObject.FindGameObjectWithTag("panel-station");

		evacPanel = GameObject.FindGameObjectWithTag("early-evac");


		//hide all of the empties
		GameObject[] canvases = {preCanvas, buildCanvas, floodCanvas, postCanvas, downtownPanel, leveePanel, chopperPanel, stationPanel, stationPanel2, stationPanel3, evacPanel};
		foreach (var canvas in canvases) canvas.SetActive(false);

		//setup the pre UI
		preCanvas.SetActive(true);
		blurPanel.SetActive(true);
	}

	void Start() {
		//assign our start variables
		cash = 1500;
		downtownLevel = 1;
		rescueLevel = 0;
		leveeLevel = 30;
		people = 500;
		//flood animation stuff
		flooder = GameObject.FindGameObjectWithTag("damlevBreak");
		fludd = (flooding) flooder.GetComponent(typeof(flooding));
		//downtown animation stuff
		dtown = GameObject.FindGameObjectWithTag("downtown-clicker");
		upDowntown = (UpgradePop) dtown.GetComponent(typeof(UpgradePop));
		//levee animation stuff
		damlev = GameObject.FindGameObjectWithTag("damlevstretch");
		duleveee = (StretchRealGood) damlev.GetComponent(typeof(StretchRealGood));

		zoom = GetComponent<zoooooooooooooooom>();

		heliMaster = GetComponent<master>();

		stationLevel = 1;

        endDay = random.Next(10, 16);
		
		rainCurve = new AnimationCurve(new Keyframe(0, 0),
		 new Keyframe(1, 1),
		 new Keyframe(0.1f , (float) random.NextDouble(0, 0.3)),
		 new Keyframe(0.1f , (float) random.NextDouble(0.1, 0.4)),
		 new Keyframe(0.2f , (float) random.NextDouble(0.2, 0.6)),
		 new Keyframe(0.3f , (float) random.NextDouble(0.4, 0.5)),
		 new Keyframe(0.4f , (float) random.NextDouble(0.2, 0.7)),
		 new Keyframe(0.5f , (float) random.NextDouble(0.1, 0.8)),
		 new Keyframe(0.6f , (float) random.NextDouble(0.5, 0.8)), 
		 new Keyframe(0.7f , (float) random.NextDouble(0.8, 0.9)),
		 new Keyframe(0.9f , (float) random.NextDouble(0.9, 1)));
		
	}

	void Update() {
		switch (gameState) {
			case state.pre:
				break;
			case state.build:
				//move through the days according to timeSpeed (player set) and deltaTime (time bet
				day += 1.0 / timeSpeed * Time.deltaTime;
				//add (cashPerDay * downtown) / timeSpeed to cash
				cash += cashPerDay * downtownLevel / timeSpeed * Time.deltaTime;
				//for each day (NOT EACH FRAME), generate random amounts of rain or drain
				//subtract drain from (rain ~1 day ago) = that will be level
				//use an AnimationCurve for randon index to amount of rain (more interesting than an evenly random distobution)
				//cash should increase people
				//if new day
				if (i == getDay() && getDay() != endDay) {
					//drain = getDrain(0);
					people += (int) Math.Round(cash) / cashPerPerson;
					i++;
				}

				if (earlyEvac && people > 0) {
					if (x > 50) {
						people--;
						saved++;
						x = 0;
					}

					x += getRelativeTimeSpeed();
				}


				//after a certain amount of days, break the levees regardless? (need to balance somehow)
				//if level > levees commence startFlood() baby
				if (day > endDay || getRain(0) > leveeLevel) {
					startFlood();
					Debug.Log("Flood started! " + rainCurve.Evaluate((float)day) + " > " + leveeLevel);
				}

				break;
			case state.flood:
				timeSpeed = 60;
				//time for rescue
				//subtract (1 / framesUntilDeath) from people; make this process slower if the levee was overflows at a later time

				//stabHimInTheNeck();

				if ((random.Next(0, 100) < 20 || rescueLevel == 0 ) && people > 0) {
					people -= 1;
					dead += 1;
				}

				fludd.breakDam();
				fludd.breakLevee();

                duleveee.breakWall();

                Debug.Log(people);
                Debug.Log(dead);


				//endGame() if saved and people are equal
				if (people <= 0) endGame();
				break;
			case state.post:
                //game has ended
                //the UI will handle the player's high score and rank in leaderboards

                //putting these here in case they save everyone so the animations won't stop halfway through
                fludd.breakDam();
                fludd.breakLevee();
                duleveee.breakWall();

                Debug.Log(people);
                Debug.Log(dead);


                break;
		}
	}

	public state getGameState() {
		//return gamestate
		return gameState;
	}

	public void startBuild() {
        //switch gameState to build mode
        preCanvas.SetActive(false);
		buildCanvas.SetActive(true);
		blurPanel.SetActive(false);
		gameState = state.build;
	}

	void startFlood() {
		//switch gameState to flood mode
		buildCanvas.SetActive(false);
		floodCanvas.SetActive(true);
		gameState = state.flood;
		zoom.resetTarget();
        duleveee.getWallsThatShallBreak();
	}

	void endGame() {
		//switch gameState to post mode
		floodCanvas.SetActive(false);
		postCanvas.SetActive(true);
		blurPanel.SetActive(true);
		gameState = state.post;
	}

	public int getDay() {
		//return day rounded to an integer
		//truncate because it isn't the fourth day until it is exactly day 4.0
		return (int) Math.Truncate(day);
	}

	public int getCash() {
		//return cash rounded to an integer
		return (int) Math.Round(cash);
	}

	public void upgradeDowntown() {
		//add one to downtown, subtract cash of downtownCost
		if (cash > getDowntownCost()) {
			cash -= getDowntownCost();
			downtownLevel += 1;
			people += 50;
			downtownCost = 200 * downtownLevel + (50 * (downtownLevel - 1));
			var addedBuildings = random.Next(2, 6);
			for (int j = 0; j < addedBuildings; j++) {
				upDowntown.MultipleAtATime();
				upDowntown.popThatBui();
			}
		}
	}

	public int getRescue() {
		//return rescue
		return rescueLevel;
	}

	public void upgradeRescue() {
		//add one to rescue, subtract cash of rescueCost
		if (cash > getRescueCost()) {
			cash -= getRescueCost();
			rescueLevel += 1;
			heliMaster.spawnHeli();
		}
	}

	public int getLevees() {
		//return levees
		return leveeLevel;
	}

	public void upgradeLevees() {
		
		//add one to rescue, subtract cash of rescueCost
		if (cash > getLeveeCost()) {
			cash -= getLeveeCost();
			leveeLevel += 10;
            duleveee.again++;
            if (duleveee.again == 7)
            {
                duleveee.Walldown();
                duleveee.Walls(duleveee.stone);
            }
            duleveee.StratchThatBui();
        }
	}

	public int getPeople() {
		return people; //add special case in future
	}

	public int getRain(int days) {
		//return rain
		return (int) (rainCurve.Evaluate((float) ((day + days) / 10)) * 150);
	}

	/* public int getDrain(int days) {
		//return drain
		var curveDrain = (int) (drainCurve.Evaluate((float) ((day + days) / 10)) * 100);
		var curveRain = getRain(days);
		if (curveDrain > curveRain)
			return curveRain;
		return curveDrain;
	}
	*/

	/* public int getLevel(int day) {
		return getRain(day) - getDrain(day);
	}*/

	public int getDowntown() {
		return downtownLevel;
	}

	public int getSaved() {
		return saved;
	}

	public void addSaved() {
        var bois = random.Next(1, 4);
		saved += bois;
		people -= bois;
	}

	public string getTime() {
		string time;
		var currentDayDecimal = day % 1.0;
		var hour = currentDayDecimal * 24;
		var minute = hour % 1.0;
		time = Math.Truncate(hour % 24 + 1) + ":" + Math.Truncate(minute * 6) + "0";
		return time;
	}

	public int getDead() {
		return dead;
	}

	int GCD(int a, int b) {
		return b == 0 ? Math.Abs(a) : GCD(b, a % b);
	}

	bool stabHimInTheNeck() {
		var gcd = GCD(rescueLevel, people);
		var deadChance = people / gcd;
		var rescueChance = rescueLevel / gcd;
		var pointer = random.Next(0, deadChance + rescueChance + 1);
		if (pointer < rescueChance * 2 * stationLevel) {
			people -= 1;
			saved += 1;
			return true;
		}

		people -= 1;
		dead += 1;
		return false;
	}

	public double getRelativeTimeSpeed() {
		return 60 / timeSpeed;
	}

	public void speedUp() {
		timeSpeed /= 2;
	}

	public void slowDown() {
		timeSpeed *= 2;
	}

	public void showDowntown() {
		hideAll();
		//canvasGroup.alpha = 0.5f;
		downtownPanel.SetActive(true);
	}

	public void showLevee() {
		hideAll();
		//canvasGroup.alpha = 0.5f;
		leveePanel.SetActive(true);
	}

	public void showChopper() {
		hideAll();
		//canvasGroup.alpha = 0.5f;
		chopperPanel.SetActive(true);
	}

	public void showStation() {
		hideAll();
		stationPanel.SetActive(true);
	}

	public void hideAll() {
		//canvasGroup.alpha = 1f;
		downtownPanel.SetActive(false);
		chopperPanel.SetActive(false);
		leveePanel.SetActive(false);
		stationPanel.SetActive(false);
	}

	public int getStationLevel() {
		return stationLevel;
	}

	public void upgradeStationLevel() {
		
		if (stationLevel == 3) {
			stationLevel++;
		} else if (cash > getStationCost() && stationLevel < 3) {
			cash -= getStationCost();
			stationLevel++;
		}

		switch (stationLevel) {
			case 1:
				stationPanel1.SetActive(true);
				stationPanel2.SetActive(false);
				stationPanel3.SetActive(false);
				break;
			case 2:
				stationPanel1.SetActive(false);
				stationPanel2.SetActive(true);
				stationPanel3.SetActive(false);
				break;
			case 3:
				stationPanel1.SetActive(false);
				stationPanel2.SetActive(false);
				stationPanel3.SetActive(true);
				GameObject.FindGameObjectWithTag("station-upgrade").GetComponentInChildren<Text>().text = "Begin Evacuating";
				break;
			case 4:
				earlyEvac = true;
				evacPanel.SetActive(true);
				break;
		}
	}

	public int getLeveeCost() {
		if (leveeLevel < 50) {
			return 100;
		} else if (leveeCost < 100) {
			return 200;
		} else {
			return 300;
		}
	}

	public int getStationCost() {
		return stationCost;
	}

	public int getRescueCost() {
		return rescueCost;
	}

	public int getDowntownCost() {
		return downtownCost;
	}

	public void restart() {
		Application.LoadLevel(0);
	}
	//use Time.deltaTime in Update() in case of lag, then commit

	//do some research, now implement types of rescue (ie boat, chopper, ambulance), each should have different rescue speeds
	//    public struct boat {
	//        public double speedMultiplier;
	//        public int capacity;
	//    }
	//add an additional type of levee (ie a dam, pump), each should have different rain and water levels
	//    public struct dam {
	//        public int height;
	//        public double leveeCost;
	//    }
	//struct{} with help with the above taskes.
	//we'll discuss more ideas

	//using this framework, we can tweak the game logic to be as interesting as possible
	//we will evetually use this code as base for our next (hopefully 3D) phases
}