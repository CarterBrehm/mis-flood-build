﻿using UnityEngine;

public class scream : MonoBehaviour {
	public epicgamer logic;

	public GameObject[] objects;

	// Start is called before the first frame update
	void Start() {
		objects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKey(KeyCode.R) && Input.GetKey(KeyCode.G) && Input.GetKey(KeyCode.B))
			foreach (var go in objects) {
				var renderers = go.GetComponentsInChildren<MeshRenderer>();
				foreach (var r in renderers) {
					foreach (var m in r.materials)
						if (m.HasProperty("_Color"))
							m.color = new Color(
								Random.Range(0f, 1f),
								Random.Range(0f, 1f),
								Random.Range(0f, 1f)
							);
				}
			}
	}
}