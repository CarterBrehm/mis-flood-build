using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = System.Random;
using Vector3 = UnityEngine.Vector3;

public class slave : MonoBehaviour {

	// random element used to delay heli takeoff
	Random random = new Random();
	
	// used to increment saved
	public epicgamer logic;

    // used to track state of each individual heli for decision making
    // ex: if heli is "moving" no new move commands will be sent to it
    public enum states {
        inactive,
        takeoff,
        wait,
        moving,
        gohome
    }

	// depending on state, heli will constantly lerp itself to the target set by master
    public Vector3 target;
    
    // used in combination with target to calculate heli speed
    public Vector3 oldTarget;

	// home position that the heli must return to during the post phase
    public Vector3 originalPosition;
    
    // helichopter number, unused currently but is unique to each helicopter
    public int index;
    
    // multipliers for heli rotation and tilting, can be adjusted to make movements more bouncy or rigid 
    float rotationSpeed = 2.0f; // default value: 2.0f
    float tiltFactor = 0.3f; // default value: 0.3f
    
    // state for the INDIVIDUAL HELICOPTER
    public states state;
    
    // used in speed calculation
    Vector3 lastPosition = new Vector3(0f, 0f, 0f);
    
    // multipliers for vector transformations and rotations
    float rotorSpeed = 1;
    public float speed = 0f;
    
	// how fast the rotors should be spinning AT LEAST, this is slowly incremented to make the rotor look like its spinning up
    public float takeOffOffset;

    // used to determine if we need to save more people here
    public bool alreadySavedHere = false;
    
	// called by the master script, kicks off the slow incrementing of takeOffOffset to spin up the rotors
    public void initiateTakeoff() {
        state = states.takeoff;
        StartCoroutine(speedUpRotors());
    }

    // takeoff routine, called async and slowly increments rotor speed, then hands control back over to master when ready
    IEnumerator speedUpRotors() {
    	// 5 times...
        for (int i = 0; i < 5; i++) {
        	// increase the rotor speed by a very small amount
            takeOffOffset += 0.05f;
            // then wait a random number of seconds between 1 and 2
            yield return new WaitForSeconds( (float) random.NextDouble(0, 2));
        }
		
		// tells the master script that this helicopter is waiting for a command
		state = states.wait;
    }

    public void Awake() {
    	// store the instantiated position in a variable so that we can return to it later
        originalPosition = gameObject.transform.position;
        // link our logic script
        logic = Camera.main.GetComponent<epicgamer>();
    }

    public void Start() {
    	// the game starts in pre/build mode, where the helicopters should be completely deactivated
        state = states.inactive;
    }

	// set by master script, sets the target, tells the heli to start moving and makes it unavailable for further commands
    public void setTarget(Vector3 desiredTarget) {
        target = desiredTarget;
        state = states.moving;
    }

    // Update is called once per frame
    void Update() {
        // calculate the speed of the helicopter and use it to set the speed of the rotors and tilt
        var position = transform.position;
        rotorSpeed = (position - lastPosition).magnitude / Time.deltaTime;
        // set the current position to a variable for the next calculation
        lastPosition = position;
        // get each child of the heli...
        foreach (Transform child in transform) {
            // depending on the tag...
            switch (child.tag) {
                case "rotor":
                    // either rotate it on the y-axis according to the calculated rotor speed...
                    child.transform.Rotate(new Vector3(0.0f, 100f, 0.0f) * (rotorSpeed + takeOffOffset));
                    break;
                case "rotor-back":
                    // or rotate it on the x-axis according to the calculated rotor speed
                    child.transform.Rotate(new Vector3(100f, 0.0f, 0.0f) * (rotorSpeed + takeOffOffset));
                    break;
            }
        }
        

        // if we're supposed to be moving...
        if (state == states.moving) {
            // move a little bit closer to the position the master script has told us to move to
            transform.position = Vector3.Lerp(transform.position, target, speed * Time.deltaTime);
            // increase the speed a little bit, because helicopters speed up over time
            speed += 0.01f;

			// ok, this is the magic box
			// literally everything happens here
			// if you touch this, I can't promise your safety
			// the below statements set the rotation of the helicopter according to two factors:
			// the x and z values of the target are made into a rotation vector for the helicopter to rotate on the y axis
			// the y value of the helicopter is determined by the rotorSpeed, which is ultimately the speed of the helicopter 
            // here we go
            
            // let's create another look target that we can mess with without changing our existing movement target
            var lookTarget = target;
            // we don't want the height of the target to influence the tilt, so let's just set the y value of our target to our current y value
            lookTarget.y = transform.position.y;
            // perfect, now lets use this magic function to turn it into a vector relative to our heli
            var lookTargetDir = Quaternion.LookRotation(lookTarget - transform.position);
            //lets tilt on the x axis (left vector) depending on the speed of the helicopter
            lookTargetDir *= Quaternion.AngleAxis(tiltFactor * rotorSpeed * -1, Vector3.left);
            // then we add in our side-to-side tilt depending on if we're turning (technically its whether we NEED to turn, but its basically the same thing)
            lookTargetDir *= Quaternion.AngleAxis(tiltFactor * (lookTarget.z - transform.position.z) * -1, Vector3.back);
            // lets apply this entie thing to the helicopter, but smoothly so we don't have any jerkiness
            transform.localRotation = Quaternion.Slerp(transform.rotation, lookTargetDir, rotationSpeed * Time.deltaTime);
            
            
			// if we're close enough to where we need to be...
            if (Vector3.Distance(transform.position, target) < 1) { 
	            // if we've moved to a new position...
	            if (!alreadySavedHere) {
		            // we've saved a person
		            logic.addSaved();
	            }
	            // we're done moving, so let's tell the master script that we're done and stop moving
                state = states.wait;
                // reset the speed back to 0 so we don't zoom off next time we go somewhere
                speed = 0f;
            }
            
            // if we're not in position...
            if (Vector3.Distance(transform.position, target) < 1) { 
	            // reset the saved flag
	            alreadySavedHere = false;
            }
        }
        
    }
}
