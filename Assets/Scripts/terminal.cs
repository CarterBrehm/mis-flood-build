﻿using UnityEngine;
using UnityEngine.UI;

public class terminal : MonoBehaviour {
	public GameObject[] choppers;
	public GameObject[] day;
	public GameObject[] dead;
	public GameObject[] downtown;
	public GameObject[] drain;
	public GameObject[] drainFuture;
	public GameObject[] drainTomorrow;
	public GameObject[] height;
	public GameObject[] level;

	public epicgamer logic;
	public GameObject[] money;
	public GameObject[] people;
	public GameObject[] rain;
	public GameObject[] rainFuture;
	public GameObject[] rainTomorrow;
	public GameObject[] saved;
	public GameObject[] state;
	public GameObject[] time;
	public GameObject[] timeSpeed;
	public GameObject[] stationLevel;
	public GameObject[] downtownUpgrade;
	public GameObject[] stationUpgrade;
	public GameObject[] leveeUpgrade;
	public GameObject[] chopperUpgrade;
	public GameObject[] stationPanels;
	
	double i = 0;
	
	void setText(GameObject[] labels, string text) {
		foreach (var label in labels) {
			var labelText = label.GetComponentInChildren<Text>();
			labelText.text = text;
		}
	}

	void Awake() {
		logic = GetComponent<epicgamer>();
		money = GameObject.FindGameObjectsWithTag("money");
		people = GameObject.FindGameObjectsWithTag("people");
		saved = GameObject.FindGameObjectsWithTag("saved");
		state = GameObject.FindGameObjectsWithTag("state");
		day = GameObject.FindGameObjectsWithTag("day");
		downtown = GameObject.FindGameObjectsWithTag("downtown");
		rain = GameObject.FindGameObjectsWithTag("rain");
		drain = GameObject.FindGameObjectsWithTag("drain");
		choppers = GameObject.FindGameObjectsWithTag("choppers");
		height = GameObject.FindGameObjectsWithTag("leveeHeight");
		time = GameObject.FindGameObjectsWithTag("time");
		dead = GameObject.FindGameObjectsWithTag("dead");
		rainTomorrow = GameObject.FindGameObjectsWithTag("rain-tomorrow");
		drainTomorrow = GameObject.FindGameObjectsWithTag("drain-tomorrow");
		rainFuture = GameObject.FindGameObjectsWithTag("rain-future");
		drainFuture = GameObject.FindGameObjectsWithTag("drain-future");
		timeSpeed = GameObject.FindGameObjectsWithTag("time-speed");
		stationLevel = GameObject.FindGameObjectsWithTag("station-level");
		downtownUpgrade = GameObject.FindGameObjectsWithTag("downtown-upgrade");
		chopperUpgrade = GameObject.FindGameObjectsWithTag("chopper-upgrade");
		leveeUpgrade = GameObject.FindGameObjectsWithTag("levee-upgrade");
		stationUpgrade = GameObject.FindGameObjectsWithTag("station-upgrade");
		stationPanels = new GameObject[3];
		stationPanels[0] = GameObject.FindGameObjectWithTag("station-panel-1");
		stationPanels[1] = GameObject.FindGameObjectWithTag("station-panel-2");
		stationPanels[2] = GameObject.FindGameObjectWithTag("station-panel-3");

		foreach (var panel in stationPanels) {
			panel.GetComponent<Image>().color = Color.white;
		}
	}

	void Update() {
		if (logic.getGameState() == epicgamer.state.build || logic.getGameState() == epicgamer.state.flood) {
			setText(money, logic.getCash().ToString());
			setText(people, logic.getPeople().ToString());
			setText(saved, logic.getSaved().ToString());
			setText(day, "Day " + (logic.getDay() + 1));
			setText(downtown, logic.getDowntown().ToString());
			setText(rain, logic.getRain(0) + "in");
			//setText(drain, logic.getDrain(0) + "in");
			setText(dead, logic.getDead().ToString());
			setText(choppers, logic.getRescue().ToString());
			setText(height, logic.getLevees().ToString());
			setText(time, logic.getTime());
			setText(rainTomorrow, logic.getRain(1) + "in");
			setText(rainFuture, logic.getRain(2) + "in");
			//setText(drainTomorrow, logic.getDrain(1) + "in");
			//setText(drainFuture, logic.getDrain(2) + "in");
			setText(timeSpeed, logic.getRelativeTimeSpeed().ToString());
			setText(stationLevel, logic.getStationLevel().ToString());
			setText(leveeUpgrade, "Upgrade - $" + logic.getLeveeCost());
			setText(chopperUpgrade, "Upgrade - $" + logic.getRescueCost());
			setText(downtownUpgrade, "Upgrade - $" + logic.getDowntownCost());
			if (logic.getStationLevel() != 3) {
				setText(stationUpgrade, "Upgrade - $" + logic.getStationCost());
			}

			switch (logic.getGameState()) {
				case epicgamer.state.pre:
					setText(state, "Phase: Pre");
					break;
				case epicgamer.state.build:
					setText(state, "Phase: Build");
					break;
				case epicgamer.state.flood:
					setText(state, "Phase: Flood");
					break;
				case epicgamer.state.post:
					setText(state, "Phase: Post");
					break;
				default:
					setText(state, "Phase: Unknown");
					break;
			}
		}

		if (logic.getLevees() - logic.getRain(0) <= 30 && logic.getRain(0) != 0) {
			if (i > 20) {
				foreach (var panel in stationPanels) {
					if (panel.GetComponent<Image>().color == Color.white) {
						panel.GetComponent<Image>().color = Color.red;
					} else {
						panel.GetComponent<Image>().color = Color.white;
					}
				}

				i = 0;
			}

			i += 8 * logic.getRelativeTimeSpeed() / (logic.getLevees() - logic.getRain(0));
		} else {
			foreach (var panel in stationPanels) {
				panel.GetComponent<Image>().color = Color.white;
			}
		}
		if (logic.getLevees() - logic.getRain(0) == 10 && logic.timeSpeed < 60) {
			logic.timeSpeed = 60;
		} 
	}
}