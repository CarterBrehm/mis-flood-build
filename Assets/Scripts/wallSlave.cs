﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSlave : MonoBehaviour
{
    //this transform is set in the master script StretchRealGood
    public Transform ldtransform;

    //an enum to easily change states and control what the wall object is doing
    public enum state { stretch, wallGoDown, changeMaterial };
    //sets the state of this object to the main thing it will be in most of the time
    public state wallState = state.stretch;

    //the scale that the wall will be stretched to
    //this could also be considered the height i guess
    public float stretchscale;

    //the width of the object
    //this is also set in the master script
    public float width;

    void Start()
    {
        //sets the scale to the scale of the the transform it is referencing
        transform.localScale = new Vector3(ldtransform.localScale.x + 5f, 0f, ldtransform.localScale.z + width);

        //sets the starting height that the walls will go to
        stretchscale = 20f;
    }

    void Update()
    {
        switch(wallState)
        {
            //when it is stretching, it will just lerp its scaled height up    
            case state.stretch:
                transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(transform.localScale.x, stretchscale, transform.localScale.z), 1f * Time.deltaTime);
                break;

            //this makes the wall shrink down then switches the object switch to the next state
            case state.wallGoDown:
                transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(ldtransform.localScale.x + 5, 0f, 0f), 5f * Time.deltaTime);
                if (transform.localScale.magnitude < new Vector3(ldtransform.localScale.x + 5, 0f, 0f).magnitude + .2f)
                {
                    wallState = state.changeMaterial;
                }
                break;

            //this last state simply makes the wall destroy itself so that it can be replaced with the next wall
            case state.changeMaterial:
                Destroy(gameObject);
                break;
        }
    }


}
