using UnityEngine;
using UnityEngine.EventSystems;

public class zoooooooooooooooom : MonoBehaviour {
	
	// reference our main logic script, which will be changing the state of our camera
	public static epicgamer logic;
	
	// hardcoded positions of each 
	Vector3 chopperPosition = new Vector3(-680f, 50f, 650f);
	Vector3 downtownPosition = new Vector3(-360f, 70f, 400f);
	Vector3 leveePosition = new Vector3(-515f, 70f, 420f);
	Vector3 stationPosition = new Vector3(-550f, 100f, 550f);

	// initialize a stateController for this camera (maybe add other cameras later)?
	public stateController state;
	
	// called by epicgamer during flood phase, returns camera to home and hides all panels
	public void resetTarget() {
		// signals to the controller to set target to home
		state.setTarget(stateController.location.home);
		// calls epicgamer to hide all panel UI
		logic.hideAll();
	}

	// Start is called before the first frame update
	void Start() {
		// sets home position to current position of camera, allows dynamic adjusting of home
		state.homePosition = gameObject.transform.position;
		
		// fills our state controller with the positions of the game elements
		state.downtownPosition = downtownPosition;
		state.chopperPosition = chopperPosition;
		state.leveePosition = leveePosition;
		state.stationPosition = stationPosition;
		
		// hook epicgamer to control UI
		logic = GetComponent<epicgamer>();
	}

	// Update is called once per frame
	void Update() {
		// if the left mouse button is clicked...
		if (Input.GetMouseButtonDown(0)) {
			// get a raycast of the mouse pointer (draw an invisible line, see what it hits)
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			// if the ray actually hits something and we're in build state...
			if (Physics.Raycast(ray, out var hit) && logic.getGameState() == epicgamer.state.build) {
				// if the mouse isn't touching a UI element
				if (!EventSystem.current.IsPointerOverGameObject()) {
					// set the target of the controller depending on the tag of what it hits
					switch (hit.transform.tag) {
						// downtown
						case "downtown-clicker":
							state.setTarget(stateController.location.downtown);
							break;
						// levee
						case "levee-clicker":
							state.setTarget(stateController.location.levee);
							break;
						// chopper
						case "chopper-clicker":
							state.setTarget(stateController.location.chopper);
							break;
						// weather station
						case "station-clicker":
							state.setTarget(stateController.location.station);
							break;
						// anything else
						case "Untagged":
							state.setTarget(stateController.location.home);
							break;
						// this should never happen, but unity complains if tHeRe Is No DeFaUlT cAsE
						default:
							state.setTarget(stateController.location.home);
							break;
					}
				}
			}
		}
		// lerp towards the corrent target of the controller, constantly. this means our camera is always ready to move and we don't have to start() it
		transform.position = Vector3.Lerp(transform.position, (Vector3) state.getTargetPosition(), 1f * Time.deltaTime);
	}

	public struct stateController {
		// used to notate target location
		public enum location {
			home,
			downtown,
			levee,
			chopper,
			station
		}
		
		// perpare all of our target positions
		public location target;
		public Vector3 downtownPosition;
		public Vector3 chopperPosition;
		public Vector3 leveePosition;
		public Vector3 homePosition;
		public Vector3 stationPosition;

		// returns current controller target
		public location getTarget() {
			return target;
		}
		
		// sets target and triggers UI appropriately
		public void setTarget(location desiredTarget) {
			target = desiredTarget;
			switch (target) {
				case location.home:
					logic.hideAll();
					break;
				case location.levee:
					logic.showLevee();
					break;
				case location.chopper:
					logic.showChopper();
					break;
				case location.downtown:
					logic.showDowntown();
					break;
				case location.station:
					logic.showStation();
					break;
			}
		}
		
		// get the position of the current target
		public Vector3? getTargetPosition() {
			switch (target) {
				case location.home:
					return homePosition;
				case location.levee:
					return leveePosition;
				case location.chopper:
					return chopperPosition;
				case location.downtown:
					return downtownPosition;
				case location.station:
					return stationPosition;
			}

			return null;
		}
	}
}