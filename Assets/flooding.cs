﻿using UnityEngine;

public class flooding : MonoBehaviour {
    //creates curve for the flooding to follow
	public AnimationCurve curve;
	public float curveTime;

    //makes it so that the dam doesn't immediately start flooding
	bool dam = false;
    //skinMeshRend that controls the dam flooding
	SkinnedMeshRenderer damFlood;

    //same as the dam starting false
	bool levee = false;
    //skinMeshRend that controls the levee flooding
	SkinnedMeshRenderer leveeFlood;

    //gets transforms to lerp for levee flood since it didn't look great originally
	public Transform levfluddheight, levfluudddd;

	public float maxSpeed = 10f; //max increased = slower end speed
	public float speed; //speed works inversely, if speed is bigger then it goes slower, if smaller goes faster
	public float speedRate = .5f; //if speed rate increased, the speed starts slower

    //sets dam to true to start the water flooding animation
	public void breakDam() {
		dam = true;
		damFlood = GameObject.FindGameObjectWithTag("breakDam").GetComponent<SkinnedMeshRenderer>();
	}

    //sets leevee to true to start the water flooding animation
	public void breakLevee() {
		levee = true;
		leveeFlood = GameObject.FindGameObjectWithTag("breakLevee").GetComponent<SkinnedMeshRenderer>();
	}

	void Update() {
        //once true, it sets the dam flood skinMeshRend on a curve to look like it's naturally flooding out
		if (dam) {
            //multiply by 100 since curve only goes from 0 to 1 but blendShapeWeight goes up to 100
			damFlood.SetBlendShapeWeight(0, curve.Evaluate(curveTime) * 100); 
            //increses curvetime so next update the damFlood shapeWeight goes higher
			curveTime += Time.deltaTime / speed;
		}

        //once true, it sets the levee flood skinMeshRend on a curve to look like it's naturally flooding out
        if (levee) {
            //lerps the levee flood water up because before it didn't look that great
			levfluudddd.position = Vector3.Lerp(levfluudddd.position, levfluddheight.position, 10f * Time.deltaTime);
            //multiply by 100 since curve only goes from 0 to 1 but blendShapeWeight goes up to 100
            leveeFlood.SetBlendShapeWeight(0, curve.Evaluate(curveTime) * 100);
            //same as with dam
            curveTime += Time.deltaTime / speed;
		}

        //if speed is less than max(10) and both levee and dam flood animations have started, it will slowly increase the speed rate so that the flooding starts fast and goes slower at the end
		if (speed < maxSpeed && levee && dam) speed += speedRate;
	}
}