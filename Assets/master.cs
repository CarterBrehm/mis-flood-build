using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class master : MonoBehaviour {
    // the first space where a heli should spawn, then adjusted by spawnHeli()
    Vector3 nextSpace = new Vector3(-677f, 22.25f, 556f);
    
    // the adjustments of the nextSpace vector, used to make a type-writer style deployment
    readonly Vector3 horizonatalOffset = new Vector3(0f, 0f, 20f);
    readonly Vector3 verticalOffset = new Vector3(-30f, 0f, 0f);

    // stores heli after instantiation
    public List<GameObject> helis = new List<GameObject>();
    
    // list of possible save positions, used by getRandomSavePosition()
    public List<GameObject> positions = new List<GameObject>();
    
    //set by Unity, cloned and positioned in spawnHeli()
    public GameObject heliPrefab;

    // amount of helicopters that have been placed in the current row
    int columnCount = 0;
    
    // used to determine when nextPosition should shift vertically
    readonly int columnMax = 8;
    
    // how many helis have been instantiated in total
    int totalHelis = 0;

    // script references
    epicgamer logic;
    slave slave;
    
    // randomizer component
    System.Random random = new System.Random();
    
    // used to keep track of if we're started the helicopters
    bool takeOffFlag = false;

    // called by epicgamer, when player upgrades rescue amount
    public void spawnHeli() {
        // instantiate a new heli and add it to our helis list
        helis.Add(Instantiate(heliPrefab, nextSpace, Quaternion.Euler(0, 90, 0)));
        
        // add our slave script to the new heli we created
        helis[totalHelis].AddComponent<slave>();

        helis[totalHelis].GetComponent<slave>().index = totalHelis;

        // increase both indices
        totalHelis++;
        columnCount++;
        
        // if we've reached the end of the row...
        if (columnCount >= columnMax) {
            // reset the horizontal position back to the beginning
            nextSpace -= (columnMax * horizonatalOffset);
            // add the vertical offset to go a row up
            nextSpace += verticalOffset;
            // then reset the amount of helis we've placed in this row
            columnCount = 0;
        }
        
        // move the position of the next heli horizontally
        nextSpace += horizonatalOffset;
    }

    // support function, used to switch all helis to takeoff mode and start all rotors
    void allTakeoff() {
        foreach (var heli in helis) {
            heli.GetComponent<slave>().initiateTakeoff();
        }
    }

    // returns the position of a random item in the positions list
    public Vector3 getRandomSavePosition() {
        return positions[random.Next(0, positions.Count)].transform.position + new Vector3(0f, random.Next(10, 30), 0f);
    }

    void Awake() {
        // fill our script variables
        logic = GetComponent<epicgamer>();
        slave = GetComponent<slave>();
        // for each house that we've marked as a save position...
        foreach (var house in GameObject.FindGameObjectsWithTag("savePosition")) {
            // add it to the list
            positions.Add(house);
        }
    }

    void Update() {
    	// if we're flooding...
        if (logic.getGameState() == epicgamer.state.flood && !takeOffFlag) {
        	// start the choppers
            allTakeoff();
            // they're already started, so lets not run this statement again
            takeOffFlag = true;
        }

		// get all the helis
        foreach (var heli in helis) {
        	// if there are any that need commands...
            if (heli.GetComponent<slave>().state == slave.states.wait) {
            	// tell them to go save some people
                heli.GetComponent<slave>().setTarget(getRandomSavePosition());
            }
        }
    }    
}
